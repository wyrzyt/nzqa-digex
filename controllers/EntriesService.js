'use strict';

var Sybase = require('sybase');
var squel = require('squel');

var logTiming = true;
var javaJarPath = './node_modules/sybase/JavaSybaseLink/dist/JavaSybaseLink.jar';
//var db = new Sybase('testase11', 5000, 'eqa_prod2', 'kevinhe', 'Mz0mft8M', logTiming, javaJarPath);


exports.getEntries = function(args, res, next) {

  var db = new Sybase('devase11', 5000, 'web_eqa2', 'kevinhe', 'Mz0mft8M', logTiming, javaJarPath);
  var qparms = { tableAliasQuoteCharacter: ""};

  console.log("/enrolments GET ============>");
  //console.log(JSON.stringify(args.results.value, 0, 2));

  var getResultsQuery = squel.select(qparms)
      .from("DIGITAL_ENTRY")
      .field("*")
      .toString();

  console.log("/results GET (query prepared) ============>");
  console.log(getResultsQuery);

  db.connect(function (err) {

    console.log("db connecting ============>");

    if (err) {
      res.statusCode = 400;
      res.end(JSON.stringify({
        "requestId": "string",
        "code": 0,
        "message": err.toString()
      }));
      return console.log(err);
    }

    console.log("db connect successful ============>");

    db.query( getResultsQuery, function (err, data) {

      console.log("db query executed <============");

      if (err) {
        console.log("db query error <============");
        console.log(err);
        res.statusCode = 400;
        res.end(JSON.stringify({
          "requestId": "string",
          "code": 0,
          "message": err.toString()
        }));
      }
      else {

        console.log("response <============");
        console.log(data);

        console.log("db disconnect <============");
        db.disconnect();

        // no response value expected for this operation
        res.statusCode = 200;
        res.end(JSON.stringify(data, 0, 2));
        console.log("/results GET complete <============");
      }

    });

  });

}
