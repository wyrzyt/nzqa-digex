'use strict';

var url = require('url');

var Results = require('./ResultsService');
var Enrolments = require('./EntriesService');

module.exports.submitResults = function submitResults (req, res, next) {
  Results.submitResults(req.swagger.params, res, next);
};

module.exports.getResults = function getResults (req, res, next) {
  Results.getResults(req.swagger.params, res, next);
}

module.exports.getEntries = function getEnrolments (req, res, next) {
  Enrolments.getEntries(req.swagger.params, res, next);
}