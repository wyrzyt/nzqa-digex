'use strict';

var Sybase = require('sybase');
var squel = require('squel');

var logTiming = true;
var javaJarPath = './node_modules/sybase/JavaSybaseLink/dist/JavaSybaseLink.jar';
//var db = new Sybase('testase11', 5000, 'eqa_prod2', 'kevinhe', 'Mz0mft8M', logTiming, javaJarPath);


exports.getResults = function(args, res, next) {

  var db = new Sybase('devase11', 5000, 'web_eqa2', 'kevinhe', 'Mz0mft8M', logTiming, javaJarPath);
  var qparms = { tableAliasQuoteCharacter: ""};

  console.log("/results GET ============>");
  //console.log(JSON.stringify(args.results.value, 0, 2));

  var getResultsQuery = squel.select(qparms)
      .from("DIGITAL_RESULT")
      .field("*")
      .toString();

  console.log("/results GET (query prepared) ============>");
  console.log(getResultsQuery);

  db.connect(function (err) {

    console.log("db connecting ============>");

    if (err) {
      res.statusCode = 400;
      res.end(JSON.stringify({
        "requestId": "string",
        "code": 0,
        "message": err.toString()
      }));
      return console.log(err);
    }

    console.log("db connect successful ============>");

    db.query( getResultsQuery, function (err, data) {

      console.log("db query executed <============");

      if (err) {
        console.log("db query error <============");
        console.log(err);
        res.statusCode = 400;
        res.end(JSON.stringify({
          "requestId": "string",
          "code": 0,
          "message": err.toString()
        }));
      }
      else {

        console.log("response <============");
        console.log(data);

        console.log("db disconnect <============");
        db.disconnect();

        // no response value expected for this operation
        res.statusCode = 200;
        res.end(JSON.stringify(data, 0, 2));
        console.log("/results GET complete <============");
      }

    });

  });

}

exports.submitResults = function(args, res, next) {
  /**
   * parameters expected in the args:
  * results (Result)
  **/
  var db = new Sybase('devase11', 5000, 'web_eqa2', 'kevinhe', 'Mz0mft8M', logTiming, javaJarPath);

  console.log("/results POST ============>");
  console.log(JSON.stringify(args.results.value, 0, 2));
  console.log("/results <============");

  var qparms = { tableAliasQuoteCharacter: ""};
  
  var query = squel.select(qparms)
      .from("LEARNER", "L")
      .field("L.nsn")
      .where("nsn = 126379328")
      .toString();

  var resultQuery = squel.select(qparms)
      .from("DIGITAL_RESULT")
      .field("*")
      .toString();  
  
  var insert = squel.insert()
      .into(".DIGITAL_RESULT")
      .set("result_code", args.results.value.totalMark)
      .set("marker_code", args.results.value.markerCode)
      .set("status", 'NEW')
      .set("learner_nsn", args.results.value.studentId)
      .set("std_number", args.results.value.assessmentId.toString())
      .set("moe_provider_id", args.results.value.providerId)
      .set("audit_tran_id", 12345)
      .toString();

  console.log("query prepared <============");
  console.log(insert);
  
  db.connect(function (err) {

    console.log("db connected callback <============");    
    
    if (err) {
      res.statusCode = 400;
      res.end(JSON.stringify({
        "requestId": "string",
        "code": 0,
        "message": err.toString()
      }));
      return console.log(err);
    }

    console.log("db connected successful <============");

    db.query( insert, function (err, data) {

      console.log("query executed (query callback)<============");

      if (err) {
        console.log("query error occured: <============");
        console.log(err);
        res.statusCode = 401;
        res.end(JSON.stringify({
          "requestId": "string",
          "code": 0,
          "message": err.toString()
        }));
      }
      else {

        console.log("response is: <============");
        console.log(data);

        console.log("db disconnect <============");
        db.disconnect();        
        
        // no response value expected for this operation
        res.statusCode = 201;
        res.end();
        console.log("no err detected <============");
      }

      console.log("query callback complete <============");

    });

    console.log("db connected passthrough <============");    
    
  });

}
